import pygame
import sympy
import sys
import math

def main (zaehler):
    pygame.init()
    a,b,h,w = 6,6,5,5
    window = pygame.display.set_mode((100*a+1,100*b+1))
    red = (255,0,0)
    white = (255,255,255)
    
    for i in range(100):
        for j in range(100):
            zaehler += 1
            if sympy.isprime(zaehler):
                print(zaehler)
                pygame.draw.rect(window, red, pygame.Rect(a*j+1, b*i+1, h, w))
            else:
                pygame.draw.rect(window, white, pygame.Rect(a*j+1, b*i+1, h, w))
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
        pygame.display.update()
        
def checkinput(x):
    
    if x < 1:
        x = 0
    else:
        x /= 10000
        x = round(x)
        x *= 10000
    return x

x = int(input("Bitte geben sie eine zahl ein"))
main(checkinput(x))