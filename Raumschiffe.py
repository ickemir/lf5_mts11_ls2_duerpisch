from pickle import TRUE
import random


class Ladung:
    def __init__(self,bezeichnung,menge):
        self.bezeichnung = bezeichnung
        self.menge = menge
        
    def getBezeichnung(self):
        return self.bezeichnung
    
    def getMenge(self):
        return self.menge
    
    def setBezeichnung(self, bezeichnung):
        self.bezeichnung = bezeichnung
        
    def setMenge(self, menge):
        self.menge = menge
        
    def whoAmI(self):
        print(type(self).__name__, self.bezeichnung, self.menge)
        
        

class Raumschiff:
    schiffe = []
    def __init__ (self,energieversorgung,schutzschilde,lebenserhaltung,huelle,photonentorpedos,reparaturandroiden,schiffsname):
        self.schiffsname = schiffsname
        self.energieversorgung = energieversorgung
        self.schutzschilde = schutzschilde
        self.lebenserhaltung = lebenserhaltung
        self.huelle = huelle
        self.photonentorpedos = photonentorpedos
        self.reparaturandroiden = reparaturandroiden
        self.ladungsverzeichnis = []
        self.logbuch = []
        self.broadcastcommunicator = ""
        Raumschiff.schiffe.append(self)
        
        
        
#Getter und Setter des Raumschiffs

    def setSchiffsname(self, name):
        self.schiffsname = name
    def getSchiffsname(self):
        return self.schiffsname
    
    def setEnergieversorgung(self, wert):
        self.energieversorgung += wert
        if self.energieversorgung > 100:self.energieversorgung = 100
    def getEnergieversorgung(self):
        return self.energieversorgung
    
    def setSchutzschilde(self, wert):
        self.schutzschilde += wert
        if self.schutzschilde > 100:self.schutzschilde = 100
        if self.schutzschilde <= 0:
            self.setEnergieversorgung(-50)
            self.setHuelle(-50)
    def getSchutzschilde(self):
        return self.schutzschilde
    
    def setLebenserhaltung(self,wert):
        self.lebenserhaltung += wert
        if self.lebenserhaltung > 100:self.lebenserhaltung = 100
    def getLebenserhaltung(self):
        return self.lebenserhaltung
    
    def setHuelle(self, wert):
        self.huelle += wert
        if self.huelle > 100:self.huelle = 100
        if self.huelle <= 0:
            self.lebenserhaltung = 0
            self.textToAll("Lebenserhaltungssysteme zerstoert")
    def getHuelle(self):
        return self.huelle
    
    def setPhotonentorpedos(self, wert):
        self.photonentorpedos += wert
    def getPhotonentorpedos(self):
        return self.photonentorpedos
    
    def setReparaturandroiden(self, wert):
        self.reparaturandroiden += wert
        if self.reparaturandroiden < 0:self.reparaturandroiden = 0
    def getReparaturandroiden(self):
        return self.reparaturandroiden
    
    def addLadung(self, bezeichnung, menge):
        self.ladungsverzeichnis.append(Ladung(bezeichnung,menge))
        self.torpedoLadung()
    def getLadung(self):
        return self.ladungsverzeichnis
    def removeLadung(self):
        for i in self.ladungsverzeichnis:
            if i.menge == 0:self.ladungsverzeichnis.pop(i)
    
    def setLogbuch(self, eintrag):
        self.logbuch.append(eintrag)
    def getLogbuch(self):
        return self.logbuch
    
    def setBroadcastcommunicator(self, nachricht):
        self.broadcastcommunicator = nachricht
        self.logbuch.append(nachricht)
    def getBroadcastcommunicator(self):
        return self.broadcastcommunicator    
    
    def photonenAbschuss(self, gegner):
        if self.getPhotonentorpedos() <= 0:self.textToAll("-=*Click*=-")
        else: 
            self.setPhotonentorpedos(-1)
            self.textToAll("Photonentorpedos abgeschossen")
            gegner.treffer()
        
    def phaserAbschuss(self, gegner):
        if self.getEnergieversorgung() < 50:self.textToAll("-=*Click*=-")
        else:
            self.setEnergieversorgung(-50)
            self.textToAll("Phaserkanone abgeschossen")
            gegner.treffer()
    
    def treffer(self):
        self.setSchutzschilde(-50)
        
    
    def textToAll(self, nachricht):
        for i in self.schiffe:i.broadcastcomminicator = nachricht
            
    def allLogs(self):
        loglist = []
        for i in self.schiffe:loglist.append(self.getLogbuch)         
    
    def reparaturAuftrag(self, schilde, energie, huelle, androiden):
        
        faktor = schilde+energie+huelle        
        if self.reparaturandroiden < androiden:androiden = self.getReparaturandroiden()        
        if schilde:self.setSchutzschilde(random.randrange(0,100)*androiden/faktor)
        if energie:self.setEnergieversorgung(random.randrange(0,100,1)*androiden/faktor)
        if huelle:self.setHuelle(random.randrange(0,100,1)*androiden/faktor)
            
    
    def torpedoLadung(self):
        for i in self.ladungsverzeichnis:
            torpedoInladung = False
            if i.bezeichnung == "Photonentorpedos":
                self.setPhotonentorpedos(i.menge)
                torpedoInladung = True
            if torpedoInladung == False:
                self.textToAll("-=*Click*=-")
                self.setBroadcastcommunicator("keine Photonentorpedos gefunden")
                print("keine Photonentorpedos gefunden")
                
    def torpedoEinsatz(self, wert):
        if wert > self.getPhotonentorpedos():return self.getPhotonentorpedos()
        else:
            self.broadcastcommunicator(wert, " Photonentorpedo(s) eingesetzt")
            print(wert, " Photonentorpedo(s) eingesetzt")    
        
        
    def getZustand(self):    
        print(self.energieversorgung," - Energieversorgung")
        print(self.schutzschilde," - Schutzschilde")
        print(self.lebenserhaltung," - Lebenserhaltung")
        print(self.huelle," - Hülle")
        print(self.photonentorpedos," - Photonentorpedos")
        print(self.reparaturandroiden," - Reparaturandroiden")
        
    
klingonen = Raumschiff(100,100,100,100,0,2,"IKS Hegh'ta")
romulaner = Raumschiff(50,100,100,100,2,2,"IRW Kazara")
vulkanier = Raumschiff(80,80,100,50,0,5,"Ni'Var")

klingonen.ladungsverzeichnis.append(Ladung("Ferengi Schnecken", 200))
klingonen.ladungsverzeichnis.append(Ladung("Klingonen Schwert", 200))
romulaner.ladungsverzeichnis.append(Ladung("Borg-Schrott", 5))
romulaner.ladungsverzeichnis.append(Ladung("Rote Materie", 2))
romulaner.ladungsverzeichnis.append(Ladung("Plasma Waffe", 50))
vulkanier.ladungsverzeichnis.append(Ladung("Forschungssonde", 35))
vulkanier.ladungsverzeichnis.append(Ladung("Photonentorpedos", 3))

klingonen.photonenAbschuss(romulaner)
romulaner.phaserAbschuss(klingonen)
vulkanier.textToAll("Gewalt is nicht logisch")
klingonen.getZustand()
klingonen.getLadung()
vulkanier.reparaturAuftrag(True,True,True,vulkanier.getReparaturandroiden())
vulkanier.torpedoLadung()
vulkanier.removeLadung()
klingonen.photonenAbschuss(romulaner)
klingonen.photonenAbschuss(romulaner)
klingonen.getZustand()
klingonen.getLadung()
vulkanier.getZustand()
vulkanier.getLadung()
romulaner.getZustand()
romulaner.getLadung()
romulaner.getLogbuch()
vulkanier.getLogbuch()
klingonen.getLogbuch()
        
    
    
